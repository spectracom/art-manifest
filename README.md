# Atomic Reference Time Manifest

This project contains the manifest to build the ART daemon and its libraries

To create a workspace:

```bash
repo init -u ssh://git@bitbucket.org/spectracom/art-manifest.git 
repo sync
make
```
